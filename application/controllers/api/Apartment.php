<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';

/*
List of functions
  ---------------
- index_get($id)
- index_post($category_id = '')
- index_put()
- index_delete()
- index_put()

*/

class Apartment extends REST_Controller {

    public $address;
    public $services;
    public $services_format;
    public $services_object;

    public $date_update;
    public $date_create;
    public $note;
    public $term;

    public function __construct()
    {
        // Construct the parent class
        parent::__construct();
        date_default_timezone_set('Asia/Ho_Chi_Minh');
        $this->load->model('apartment_model');
        $this->load->helper('services');
        $this->load->library('format');

        /* Address handle */
        $this->address = get_object_address();

        /* services Handle*/
        // it same with column name in table 'apartments'. services section.
        $this->services = get_services_list(); // array()
        $this->services_object = get_services_object_list();
        // print_r(get_services_object_list()); die;

        /* contract Term Handle */
        $this->term = get_term_format();
    }


    /**
     * get all apartments
     *
     * For more details, see LINK TSD will be added here, dont worry developer
     *
     * @return  a response
     */
    public function index_get($id = null)
    {
        // Screenshot demo https://prnt.sc/ras8wg
        $apm_list = $this->apartment_model->get($id);

        if($apm_list)
        {
            foreach ($apm_list as $index => $row) 
            {
                foreach ($row as $field_name => $value) 
                {
                    $apm_list[$index][$field_name] = json_decode($value,true);
                }   
            }

            $this->response($apm_list, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
        else
        {
            return $this->response([
                'status' => FALSE,
                'message' => 'No apartment were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }


    public function index_post()
    {
        $data = array();
        /*
        List of key value in postman should be apply. This based on $this->post(key)

            'Address'
            - number
            - street
            - ward
            - district
            - city

        For each service information in '$services' (line 46) . example:
            - Screenshot from postman: https://prnt.sc/ras3yt

            - electricity__avalable
            - electricity__unit_price
            - electricity__units
            - electricity__unit_type
            - electricity__detail

            - water__avalable
            - water__unit_price
            - water__units
            - water__unit_type
            - water__detail
            
            - room_cleaning__avalable
            - room_cleaning__unit_price
            - room_cleaning__units
            - room_cleaning__unit_type
            - room_cleaning__detail
            ... do it for any '$services' element :)

        */

        /* Address section */
        foreach ($this->address as $field_name => $value) {
            $this->address->$field_name = $this->post($field_name) != null ? 
                        $this->post($field_name) : $this->address->$field_name;
        }

        $data['address'] = $this->format->to_json($this->address);

        /* Services fields section */
        // print($this->post()); die;
        foreach ($this->post as $name => $value) {
            $split_name = explode('__', $name);
            $field_name = $split_name[0];
            $field_property = $split_name[1];
        }
        $i = 0;
        foreach ($this->services_object as $index => $service) {
            $name = $this->services[$i];
            
            $service->$name->avalable = $this->post($name .'__avalable') != null?
                                        $this->post($name .'__avalable') : $service->$name->avalable;

            $service->$name->unit_price = $this->post($name .'__unit_price') != null?
                                        $this->post($name .'__unit_price') : $service->$name->unit_price;

            $service->$name->units = $this->post($name .'__units') != null?
                                        $this->post($name .'__units') : $service->$name->units;

            $service->$name->unit_type = $this->post($name .'__unit_type') != null?
                                        $this->post($name .'__unit_type') : $service->$name->unit_type;

            $service->$name->detail = $this->post($name .'__detail') != null?
                                        $this->post($name .'__detail') : $service->$name->detail;
            $data[$name] = $this->format->to_json($service->$name);
            $i+= 1;
        }

        /* data insert section */
        $data['term'] = get_term_format();
        $data['date_create'] = $this->format->to_json(date('d-m-Y'));


        $success = $this->apartment_model->add($data); // insert to database.

        if($success)
        {
            return $this->response([
                'status' => TRUE,
                'message' => 'successful'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
        else
        {
            return $this->response([
                    'status' => FALSE,
                    'message' => 'fail'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    public function index_put()
    {
        if(count($this->put())) // if have any field
        {
            $data['id'] = $this->put('apm_id');
            $put_arr = $this->put();
            array_pop($put_arr);
            foreach ($put_arr as $field_name => $value) 
            {
                $field_form_name = explode("__", $field_name);
                if(in_array($field_form_name[0], get_services_list()))
                {
                    $data['value'] = $value;
                    $data['field_name'] = $field_form_name[0];
                    $data['property'] = $field_form_name[1];
                    $success = $this->apartment_model->update($data);
                    if($success)
                    {
                        $this->response([
                            'status' => TRUE,
                            'message' => 'successful'
                        ], REST_Controller::HTTP_NOT_FOUND);
                    }
                    else
                    {
                        return $this->response([
                                'status' => FALSE,
                                'message' => 'fail at apartments.id = '.$data['id']
                        ], REST_Controller::HTTP_NOT_FOUND);
                    }
                }
                else
                {
                    switch ($field_name) 
                    {
                        case 'address':
                            return $this->response([
                                'status' => FALSE,
                                'message' => 'this field is not develope'
                            ], REST_Controller::HTTP_NOT_FOUND);
                            break;
                        case 'date_create':
                            return $this->response([
                                'status' => FALSE,
                                'message' => 'this field is not develope'
                            ], REST_Controller::HTTP_NOT_FOUND);
                            break;
                        case 'date_update':
                            return $this->response([
                                'status' => FALSE,
                                'message' => 'this field is not develope'
                            ], REST_Controller::HTTP_NOT_FOUND);
                            break;

                        case 'term':
                            return $this->response([
                                'status' => FALSE,
                                'message' => 'this field is not develop'
                            ], REST_Controller::HTTP_NOT_FOUND);
                            break;
                        
                        default:
                            return $this->response([
                                'status' => FALSE,
                                'message' => 'this field is not existed in database'
                            ], REST_Controller::HTTP_NOT_FOUND);
                            break;
                    }
                }
            }
        }
            
    }

    public function index_delete()
    {
        if(count($this->delete()))
        {

        }
    }

}

/* End of file A.php */
/* Location: ./application/controllers/api/A.php */