<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if ( ! function_exists('get_services_list'))
{
	/**
	 * 
	 *
	 * @return	array()
	 */
	function get_services_list()
	{
		$path_json_file = "application/config/resources/services.json";
		$data = file_get_contents($path_json_file);

		return json_decode($data, true);
	}
}

if ( ! function_exists('get_service_format'))
{
	/**
	 * 
	 *
	 * @return	array()
	 */
	function get_service_format()
	{
		$path_json_file = "application/config/resources/service_format.json";
		$data = file_get_contents($path_json_file);

		return $data;
	}
}

if ( ! function_exists('get_services_object_list'))
{
	/**
	 * 
	 *
	 * @return	json()
	 */
	function get_services_object_list()
	{
		$services_list = get_services_list();
		$service_format = get_service_format();
		$services_object = '[';

		foreach ($services_list as $i) 
		{
           $services_object.= '{"' .$i. '":' . $service_format. '},';
        }

        $services_object = rtrim($services_object, ",");
        $services_object.= ']';
        $services_object_list = json_decode($services_object);

		return $services_object_list;
	}
}

if ( ! function_exists('get_term_format'))
{
	/**
	 *
	 * @return	array()
	 */
	function get_term_format()
	{
		$path_json_file = "application/config/resources/term_contract_format.json";
		$data = file_get_contents($path_json_file);
		return $data;
	}
}


if ( ! function_exists('get_object_address'))
{
	/**
	 * 
	 *
	 * @return	array()
	 */
	function get_object_address()
	{
		$path_json_file = "application/config/resources/address_format.json";
		$data = file_get_contents($path_json_file);
		$data = json_decode($data);

		return $data;
	}
}

if ( ! function_exists('set_service_object'))
{
	/**
	 * 
	 *
	 * @return	array()
	 */
	function set_service_object($field, $property, $value)
	{
		

		return $data;
	}
}
