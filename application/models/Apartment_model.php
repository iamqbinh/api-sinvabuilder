<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apartment_model extends CI_Model {

	private $table;	
	public function __construct()
    {
    	parent::__construct();
    	$this->table = 'apartments';
    }

	public function get($id = null)
	{
		if ($id != null)
		{
    		$query = $this->db->get_where($this->table, array('id' => $id));
    		return count($query->result_array()) > 0 ? $query->result_array(): false;
		}
		else
		{
			$query = $this->db->get_where($this->table);
    		return count($query->result_array()) > 0 ? $query->result_array(): false;
		}
	}

    public function add($data)
	{
	    return $this->db->insert($this->table, $data) === true ? true : false; // return true | false
	}

	public function update($data)
	{
		$query = "UPDATE ".$this->table."
				  SET ".$data['field_name']." = 
				  JSON_REPLACE( ".$data['field_name'].", 
				  			 '$.".$data['property']."', 
				  			  '".$data['value']."')
				  WHERE id = ".$data['id'];

		return $this->db->simple_query($query) === true ? true : false;
	}

}


/* End of file apartment.php */
/* Location: ./application/models/apartment.php */