<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function __construct()
	{
		parent::__construct();
		$table = 'users';
	}

	/**
	 * Verify user login information
	 *
	 */
	protected function verifyAccount($data) {

		$user = $data['username'];
		$password = $data['password'];

		$query = $this->db->query('SELECT * FROM '.$table.' 
						WHERE account = '.$user.' 
						AND password = "'.$password.'"');

		if($query->num_rows())
			return $query->result_array();
		else
			return false;
	}
	

}


/* End of file Login_model.php */
/* Location: ./application/models/Login_model.php */