<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Room_model extends CI_Model {
	public function __construct()
	{
		parent::__construct();
		$table = 'rooms';
		
	}

    public function Add($data)
	{
	    $this->db->insert($this->$table, $data);
	}
	
	public function Update($room_id , $field_name, $field_value)
	{

		$data = array($field_name => $field_value);
		$this->db->where('id', $room_id);
		$this->db->update($this->$table, $data);

	}
	public function Delete($id)
	{
		$this->db->where("id", $id);
		$this->db->delete($this->$table);
	}
	
	public function deleteRoomByApm($apm_id)
	{
		$this->db->where("apartment_id", $apm_id);
		$this->db->delete($this->$table);
	}

	public function getListRoomById($id)
	{
		if($id){
			$query = $this->db->query('SELECT * 
								   FROM '.$this->$table.
								   ' WHERE '.$this->$table.'.apartment_id = '.$id);
			return $query->result_array();
		}
		else return;
		
	}
	
}

/* End of file Room_model.php */
/* Location: ./application/models/Room_model.php */